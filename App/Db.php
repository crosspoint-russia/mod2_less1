<?php
/*
 * Улучшите класс Db. Сейчас мы написали только метод query(),
 * предназначенный для выполнения запросов, возвращающих данные.
 * Напишите еще один метод execute($query, $params=[]),
 * который будет выполнять запросы, не возвращающие данные (например INSERT, UPDATE)
 * и возвращать лишь true/false в зависимости от того, удачно ли выполнился запрос.
*/
namespace App;

class Db
{

    protected $dbh;

    public function __construct()
    {
        $dsn = 'mysql:host=localhost;dbname=php2';
        $user = 'root';
        $password = '';
        $this->dbh = new \PDO($dsn, $user, $password);
    }

    public function execute($query, $params=[])
    {
        $sth = $this->dbh->prepare($query);
        return $sth->execute($params);
    }

    public function query($sql, $data = [], $class = null)
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($data);
        if (false === $res) {
            $errors = implode(', ', $sth->errorInfo());
            die('DB error in ' . $sql . ' -> ' . $errors);
        }
        if (null === $class) {
            return $sth->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        }
    }

}