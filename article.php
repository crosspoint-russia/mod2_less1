<?php

require __DIR__ . '/autoload.php';

if (true === isset($_GET['id'])) {
    $article = \App\Models\Article::findById((int)$_GET['id']);

    if (false === $article) {
        header('HTTP/1.0 404 Not Found');
        die();
    }

    include __DIR__ . '/template/article.php';

} else {
    header('Location: /');
    die();
}