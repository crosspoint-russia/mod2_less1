-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 10 2017 г., 19:13
-- Версия сервера: 5.5.50
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `php2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` bigint(20) unsigned NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`) VALUES
(1, 'Евгений', 'Киселёв');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `text`) VALUES
(1, 'Бывший вице-президент России предрек Порошенко судьбу Хусейна', 'Президент Украины Петр Порошенко должен разделить судьбу иракского лидера Саддама Хусейна, поскольку повинен в гибели людей, считает генерал-майор авиации, бывший вице-президент России Александр Руцкой. На его взгляд, Украину как государство ждет распад, а Донбасс — независимость.\r\n'),
(2, 'Семью террориста из Иерусалима лишили вида на жительство в Израиле', 'Семью террориста Фади Аль-Кунбара, совершившего наезд на военных Армии обороны Израиля (ЦАХАЛ), лишили вида на жительство в Израиле. Об этом заявил министр внутренних дел страны Арье Дери в интервью радиостанции «Галей ЦАХАЛ», его слова передает ресурс Ynetnews.\r\n\r\n'),
(3, 'Суд оправдал коммунистов за цветы Сталину', 'Пятигорский суд оправдал пятерых активистов КПРФ и прекратил административные дела о несанкционированном митинге, организованном к годовщине со дня рождения Иосифа Сталина. Об этом во вторник, 10 января, сообщает ТАСС.\r\n\r\n«На основании исследованных материалов суд производство по вышеуказанным делам прекратил за отсутствием события административного правонарушения», — сказала пресс-секретарь суда Елена Ярошенко.\r\n\r\n'),
(4, 'Волонтер сообщил об удачной вылазке ВСУ в районе Дебальцево', 'Украинский волонтер Юрий Мысягин сообщил, что Вооруженные силы Украины (ВСУ) в районе Дебальцево заняли более выгодные позиции. Об этом во вторник, 10 января, он написал на своей странице в Facebook.\r\n\r\n');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
